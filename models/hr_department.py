from odoo import api, fields, models


class HrDepartment(models.Model):
    _inherit = 'hr.department'

    dingtalk_id = fields.Char(string='钉钉id')

    def current_departments(self):
        depts = self.sudo().search([])
        dept_ids = {int(dept.dingtalk_id): dept for dept in depts if dept.dingtalk_id}
        return dept_ids

    def add_department_list(self, dept_list=[]):
        if not dept_list: return
        dept_ids = self.current_departments()
        for dept in dept_list:
            dept_id = dept['dept_id']
            parent_id = dept['parent_id']
            if dept_id in dept_ids.keys(): continue
            obj = self.create({
                'name': dept['name'],
                'dingtalk_id': dept['dept_id'],
                'parent_id': dept_ids[parent_id].id if parent_id in dept_ids.keys() else None
            })
            dept_ids[obj.dingtalk_id] = obj

    def update_department_list(self, dept_list=[]):
        pass